window.addEventListener('DOMContentLoaded', async () =>{
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);
    if(response.ok) {
        const data =  await response.json();
        const selectTag = document.getElementById('location');
        console.log(data.locations)
        for(let location of data.locations){
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.append(option)
        }
    }
    const form = document.getElementById('create-conference-form')
    form.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(form);
        const conferenceURL = 'http://localhost:8000/api/conferences/';
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)
        const fetchOptions = {
            method: "post",
            body: json,
            headers: {
              'Content-Type': 'application/json',
            },
          };
        const response = await fetch(conferenceURL,fetchOptions);
        if(response.ok){
            form.reset();
            const newConference = await response.json();
            console.log(newConference)
        }
    });
});
