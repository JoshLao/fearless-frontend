function createCard(name, description,pictureUrl,locationName,startDate,endDate) {
    return `
    <div class='col-4 p-2'>
        <div class="card shadow-lg p-3 ">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-body-secondary">${locationName}</h6>
            <p class="card-text">${description}</p>
            <div class='card-footer bg-transparent border-success'>
            ${startDate.toLocaleString('en-US')} -
            ${endDate.toLocaleString('en-US')}
            </div>
            </div>
        </div>
    </div>
    `;
}
function createPlaceHolder(){
  return`<div class="card w-25 h-75 place-holder" aria-hidden="true">
  <img src="..." class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title placeholder-glow">
      <span class="placeholder col-6"></span>
    </h5>
    <p class="card-text placeholder-glow">
      <span class="placeholder col-7"></span>
      <span class="placeholder col-4"></span>
      <span class="placeholder col-4"></span>
      <span class="placeholder col-6"></span>
      <span class="placeholder col-8"></span>
    </p>
    <a class="btn btn-primary disabled placeholder col-6" aria-disabled="true"></a>
  </div>
</div>`
}
function createErrorTag(e){
  return `<div class="alert alert-danger d-flex align-items-center" role="alert">
  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
  <div>
    ${e}
  </div>
</div>`
}
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const row = document.querySelector('.row');
    try {

        const response = await fetch(url);

      if (!response.ok) {
        throw new Error("Something went wrong.")
      } else {

        const data = await response.json();
        for (let conference of data.conferences) {
          row.innerHTML+=createPlaceHolder();
        }
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          console.log(conference.id)
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts)
            const endDate = new Date(details.conference.ends)
            const locationName = details.conference.location.name
            const html = createCard(title, description, pictureUrl,locationName,startDate,endDate);
            row.innerHTML += html;
          }
          const placeholder = document.querySelector('.place-holder')
          placeholder.remove()
        }


      }
    } catch (e) {
      // Figure out what to do if an error is raised
      row.innerHTML+=createErrorTag(Error("Something went wrong!"))
    }

  });
